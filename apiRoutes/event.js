const express = require('express');
const uuidv1 = require('uuid/v1');
const eventRouter = express.Router();
const moment = require('moment');

const Event = require('../models/Event');
const Ticket = require('../models/Ticket');
const ObjectId = require('mongoose').Types.ObjectId;

moment.locale('es');

const getResumedPaymentsData = (paymentsData) => {
    let result = {
        paymentMethods: {
            "PSE": 0,
            "EFECTIVO": 0,
            "TARJETA-CREDITO": 0
        },
        paymentStages: {}
    }
    paymentsData.map((paymentElm) => {
        if (!result.paymentStages[paymentElm.payment.value]) {
            result.paymentStages[paymentElm.payment.value] = 1
        } else {
            result.paymentStages[paymentElm.payment.value] = result.paymentStages[paymentElm.payment.value] + 1;
        }
        switch(paymentElm.payment.entity.toLowerCase()) {
            case "efecty":
                result.paymentMethods.EFECTIVO = result.paymentMethods.EFECTIVO + 1;
                break;
            case "pse":
                result.paymentMethods.PSE = result.paymentMethods.PSE + 1;
                break;
            case "gana":
                result.paymentMethods.EFECTIVO = result.paymentMethods.EFECTIVO + 1;
                break;
            default:
                result.paymentMethods["TARJETA-CREDITO"] = result.paymentMethods["TARJETA-CREDITO"] + 1;
                break;
        }
    });
    return result;
}

eventRouter.post('/', async (req, res) => {
    const newEvent = new Event(req.body);
    newEvent.save().then((record) => {
        res.status(200).json({ status: 'Created', record });
    }).catch((error) => {
        res.status(500).json({ status: 'An error ocurred', error });
    });
});

eventRouter.put('/', async(req, res) => {
    const newEvent = req.body;
    Event.findByIdAndUpdate(newEvent._id, newEvent).then((newRecord) => {
        res.status(200).json({ status: 'success', message: 'Event updated', data: newRecord });
    }).catch((error) => {
        res.status(500).json({ status: 'An error ocurred', error });
    });
})

eventRouter.get('/', (req, res) => {
    let isAdmin = req.query.isAdmin;
    let filter = {};
    if (!isAdmin) {
        filter = {status: 'active'}
    }
    Event.find(filter, (error, data) => {
        if (error == null) {
            let events = [];
            data.filter((event) => {
                if (moment(event.endDate).isSame(moment(moment.now()), 'd') || moment(event.endDate).isAfter(moment.now()) ) {
                    events.push(event);
                }
            });
            res.status(200).json({ status: 'Consulted', data: events });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
});

eventRouter.get('/getAll', (req, res) => {
    let isAdmin = req.query.isAdmin;
    let filter = {};
    if (!isAdmin) {
        filter = {status: 'active'}
    }
    Event.find(filter, (error, data) => {
        if (error == null) {
            let events = [];
            data.filter((event) => {
                if (moment(event.endDate).isSame(moment(moment.now()), 'd') || moment(event.endDate).isAfter(moment.now()) ) {
                    events.push(event);
                }
            });
            res.status(200).json({ status: 'Consulted', data: events });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
});

eventRouter.delete('/:id', async (req, res) => {
    let eventId = req.params.id;
    let tickets = await Ticket.find({ event: new ObjectId(eventId) });
    if (tickets.length == 0) {
        let event = await Event.findById(eventId);
        event.status = 'inactive';
        await event.save();
        res.status(200).json({ status: 'success', message: 'Event deleted' });
    } else {
        res.status(200).json({ status: 'An error ocurred', message: 'The event cant be deleted' });
    }
});

eventRouter.post('/activate', async (req, res) => {
    let eventId = req.body.eventId;
    let event = await Event.findById(eventId);
    event.status = 'active';
    await event.save();
    res.status(200).json({ status: 'success', message: 'event-deleted' });
});

eventRouter.get('/:id', (req, res) => {
    let id = req.params.id;
    Event.find({userId: id}, (error, data) => {
        if (error == null) {
            res.status(200).json({ status: 'Consulted', data });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
});

eventRouter.get('/statistics-free/:eventId', async(req, res) => {
    let { eventId } = req.params;
    Ticket.find({ event: new ObjectId(eventId) }).populate('user').exec((error, data) => {
        if (error == null) {
            res.status(200).json({ status: 'Consulted', data } );
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
})

eventRouter.get('/statistics/:eventId', async (req, res) => {
    let { eventId } = req.params;
    Ticket.find({ event: new ObjectId(eventId) }).populate('user').populate('payment').exec((error, data) => {
        if (error == null) {
            let resumedPaymentsData = getResumedPaymentsData(data);
            res.status(200).json({ status: 'Consulted', data, resumedPaymentsData } );
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
})

eventRouter.post('/guestlist', (req, res) => {
    let { userName, userDoc, eventId, userEmail, userCellphone, question1, question2, especial } = req.body;
    Event.findOne({ _id: new ObjectId(eventId) }).then((doc) => {
        if (doc && doc.guestlist > 0) {
            let exist = false;
            doc.guestlistUsers.map((elm) => {
                if (elm.Cedula == userDoc) exist = true;
            })
            if (!exist) {
                if (doc.guestlistQuantity > doc.guestlistUsers.length) {
                    if (especial) {
                        doc.guestlistUsers.push({
                            Nombre: userName,
                            Cedula: userDoc,
                            Email: userEmail,
                            Celular: userCellphone,
                            Pregunta1: question1,
                            Pregunta2: question2
                        });
                    } else {
                        doc.guestlistUsers.push({
                            Nombre: userName,
                            Cedula: userDoc,
                            Email: userEmail,
                            Celular: userCellphone
                        });
                    }
                    
                    doc.save();
                    res.json({ status: 'success', message: 'Se inscribio el usuario al guestlist' });
                } else {
                    res.json({ status: 'guestlist closed', message: 'La pista de baile está llena, Los cupos disponibles para ingresar por lista se han terminado' });
                }
            } else {
                res.json({ status: 'already registered', message: 'El usuario ya existe en la lista' });
            }
        } else {
            res.json({ status: 'fail', error: 'No se encontro el evento o no es posible ponerte en lista' });
        }
        res.end();
    });
});

eventRouter.get('/currentSoldTickets/:eventId/:stageNumber', async(req, res) => {
    let { stageNumber, eventId } = req.params;
    let eventToQuery = await Event.findById(eventId);
    let ticketing = eventToQuery.tickets[stageNumber];
    let response = { ticketsToSell: ticketing.amount_available, ticketsSold: ticketing.amount_sold };
    res.json({ status: 'success', data: response });
})

eventRouter.post('/verifyPendingTicket', async(req, res) => {
    let { eventId, userId } = req.body;
    Ticket.findOne({ user: new ObjectId(userId), event: new ObjectId(eventId) }).then((doc) => {
        if (doc) {
            res.json({ status: 'success', message: 'Ticket was found', data: doc });
        } else {
            res.status(500).json({ status: 'fail', error: 'No se ha recibido el pago' });
        }
    });
})

eventRouter.post('/validateTicket', async(req, res) => {
    const { code } = req.body;
    Ticket.findOne({ code }).then((doc) => {
        if (doc) {
            if (doc.status == "Por redimir") {
                doc.status = "Redimido";
                doc.save();
                res.json({ status: 'success', message: 'El ticket fue redimido correctamente'});
            } else {
                res.json({ status: 'fail', error: 'El ticket ya fue redimido' });
            }
        } else {
            res.json({ status: 'fail', error: 'No se encontro el ticket' });
        }
        res.end();
    });
});

eventRouter.post('/getFreeTicket', async (req, res) => {
    const { questions, eventId, userId } = req.body;
    const ticketCode = uuidv1();
    const ticket = new Ticket({
        user: userId,
        event: eventId,
        code: ticketCode,
        status: 'Por redimir',
        createdAt: new Date(),
        questions
    });
    ticket.save();
    res.json({ status: 'success', qr: ticketCode });
});

module.exports = eventRouter;