const express = require('express');
const uuidv1 = require('uuid/v1');
const qrCode = require('yaqrcode');
const inlineBase64 = require('nodemailer-plugin-inline-base64');

const paymentRouter = express.Router();
const EpaycoManager = require('../utils/epaycoManager');
const epaycoManager = new EpaycoManager();
const WompiClient = require('../utils/wompiClient');
const wompiClient = new WompiClient(process.env.PRODUCTION == "true" ? "production" : "test");
const Payment = require('../models/Payment');
const Event = require('../models/Event');
const Ticket = require('../models/Ticket');
const User = require('../models/User');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'edancevent@gmail.com',
        pass: 'valentino83010'
    }
});

transporter.use('compile', inlineBase64());

let mailOptions = {
    from: 'edancevent@gmail.com',
    to: 'mauricioatencia@edancevent.com',
    subject: 'Un nuevo organizador quiere unirse a EDE',
    text: '',
    html: ''
};

const sendEmailWithTicketToUser = (qrCodeNumber, invoiceReference, eventName, eventImageSrc, userEmail) => {
    let ticketQr = qrCode(qrCodeNumber);
    let emailHtml = 
    `<p>La compra del ticket para el evento - ${eventName} - ha sido exítosa, Su referencía de pago es: ${invoiceReference} <br /> a continuación el código QR que debe presentar el día del evento. <br /></p>
    <img src="${ticketQr}" height="250" width="250" />
    <br />
    <img src="${eventImageSrc}" height="250" width="250" />
    <br />
    <P>Preparate para bailar!</p>
    <p>ELECTRONIC DANCE EVENT.</p>
    `;
    mailOptions.subject = "Transacción aprobada - Gracias por confiar en EDE - Tu ticket esta listo: ";
    mailOptions.to = userEmail;
    mailOptions.html = emailHtml;
    transporter.sendMail(mailOptions);
}

paymentRouter.post('/createCreditCard', async (req, res) => {
    let { cardNumber, expMonth, expYear, cvc, userId, userName, userEmail } = req.body;

    let creditCardData = { 
        number: cardNumber, 
        expMonth, 
        expYear, 
        cvc,
        name: userName,
        userEmail
    };

    let response = await wompiClient.tokenizeCreditCard(creditCardData);
    res.json({ status: 'success', data: { ...response }});
})

paymentRouter.post('/buyTicketCreditCard', async (req, res) => {
    let {
        userId, eventId, value, token, userEmail, stageNumber, paymentMethod, installments, eventName, eventImageSrc
    } = req.body;

    let invoiceReference = uuidv1();

    let paymentData = {
        value: value + "00",
        userEmail,
        paymentMethod,
        reference: invoiceReference,
        installments,
        token
    }

    let transaction = await wompiClient.createTransaction(paymentData);
    let transactionId = transaction.response.data.id;
    setTimeout( async () => {
        let transactionStatusData = await wompiClient.getTransactionStatus(transactionId);
        let response = JSON.parse(transactionStatusData.response);
        let transactionStatus = response.data.status;
        if (transactionStatus == "APPROVED") {
            const payment = new Payment({
                createdDate: new Date().toString(),
                invoice: invoiceReference,
                wompiRef: response.data.id,
                eventId,
                userId,
                entity: paymentMethod + "-" + response.data.payment_method.extra.brand,
                value,
                status: 'Aceptada'
            });
            payment.save().then(async (doc) => {
                const ticket = new Ticket({
                    user: userId,
                    event: eventId,
                    payment: doc._id,
                    code: response.data.id,
                    createdAt: new Date(),
                    status: 'Por redimir'
                });
                await ticket.save();
                let eventTicketing = await Event.findById(eventId);
                eventTicketing.tickets[stageNumber].amount_sold = eventTicketing.tickets[stageNumber].amount_sold + 1;
                await eventTicketing.updateOne(eventTicketing);
                sendEmailWithTicketToUser(response.data.id, invoiceReference, eventName, eventImageSrc, userEmail);
                res.json({ status: 'success', qr: response.data.id });
            });
        } else if (transactionStatus == "DECLINED") {
            res.json({ status: 'declined', message: response.data.status_message });
        } else if (transactionStatus == "ERROR") {
            res.json({ status: 'error' });
        } else if (transactionStatus == "PENDING") {
            res.json({ status: 'pending', transactionId: response.data.id });
        }
    }, 8000);

});

paymentRouter.post('/buyTicketPSE', async (req, res) => {
    let { bank, eventName, eventId, userId, value, userDoc, userName, userLastname, userCellphone, userEmail } = req.body;
    let invoice = uuidv1();
    let urlResponse = process.env.CLIENT_URL+`/paymentResponse/${invoice}/${userId}`;
    let PSEinfo = {
        bank: bank,
        invoice: invoice,
        description: eventName,
        value: String(value),
        tax: "0",
        tax_base: "0",
        currency: "COP",
        type_person: "0",
        doc_type: "CC",
        doc_number: userDoc,
        name: userName,
        last_name: userLastname,
        email: userEmail,
        country: "CO",
        cell_phone: userCellphone,
        url_response: urlResponse,
        url_confirmation: process.env.BACKEND_URL+"/api/payments/paymentValidation",
        method_confirmation: "POST",
    }
    epaycoManager.chargeByPSE(PSEinfo).then(async (response) => {
        if (response.success == true && response.title_response == "SUCCESS") {
            let { factura, ref_payco, transactionID, urlbanco, estado } = response.data;
            const payment = new Payment({
                createdDate: new Date().toString(),
                invoice: factura,
                epaycoRef: ref_payco,
                transactionID,
                eventId,
                userId,
                entity: "PSE",
                value,
                status: estado
            });
            await payment.save();
            res.json({ status: 'success', data: { factura, epaycoRef: ref_payco, transactionID, urlbanco } });
        } else {
            res.json({ status: 'An error ocurred', error: "Ocurrio un error creando la transacción" });
        }
    }).catch((error) => {
        res.json({ status: 'An error ocurred', error: error.toString() });
    })
});

paymentRouter.get('/transactionStatus', async (req, res) => {
    let { userId, invoice } = req.query;
    try {
        let payment = await Payment.findOne({ invoice, userId });
        let transactionStatus = await epaycoManager.getTransactionStatus(payment.transactionID);
        console.log("transaction status", transactionStatus);
        res.status(200).json({ status: 'Transaction consulted', data: transactionStatus });
    } catch(error) {
        res.status(500).json({ status: 'An error ocurred', message: 'The invoice reference passed doesnt exist' });
    }
});

paymentRouter.post('/buyTicketCash', async (req, res) => {
    let { eventId, userId, entity, eventName, value, userName, userLastname, userEmail, userCellPhone, userDoc } = req.body;
    const invoice = uuidv1();
    console.log(value);
    let cash_info = {
        invoice: invoice,
        description: eventName,
        value: String(value),
        tax: "0",
        tax_base: "0",
        currency: "COP",
        type_person: "0",
        doc_type: "CC",
        doc_number: userDoc,
        name: userName,
        last_name: userLastname,
        email: userEmail,
        cell_phone: userCellPhone,
        end_date: "2019-12-15",
        url_response: process.env.BACKEND_URL+"/api/payments/paymentValidation",
        url_confirmation: process.env.BACKEND_URL+"/api/payments/paymentValidation",
        method_confirmation: "POST"
    }
    epaycoManager.chargeByCash(entity, cash_info).then(async (response) => {
        let { factura, ref_payco, valor, pin, codigoproyecto, estado } = response.data;
        console.log("respuesta de solicitar pin", response);
        console.log(response.data.errores)
        if (response.success == true) {
            const payment = new Payment({
                createdDate: new Date().toString(),
                invoice: factura,
                epaycoRef: ref_payco,
                eventId,
                userId,
                entity,
                value: valor,
                pin: pin,
                projectCode: codigoproyecto,
                status: estado
            });
            await payment.save();
            let responseToSend = {
                pin: pin,
                projectCode: codigoproyecto
            };
            mailOptions.subject = "Datos para realizar el pago de su ticket con EDE";
            mailOptions.to = userEmail;
            mailOptions.text = `Dirijase a un punto ${entity.toUpperCase()} y realice su pago con los siguientes datos: 
            CODIGO DEL PROYECTO: ${codigoproyecto}
            NUMERO DE PIN: ${pin}
            pasados 10 minutos de recibido el pago puedes verificar tu ticket en la sección 'Mis tickets'`;
            transporter.sendMail(mailOptions);
            res.json({ status: 'success', data: responseToSend });
        }
    }).catch((e) => {
        res.json({ status: 'An error ocurred', error: e });
    })
});

paymentRouter.post('/paymentValidation', async (req, res) => {
    let { 
        x_ref_payco,
        x_id_invoice,
        x_response,
        x_cod_response    
    } = req.body;
    Payment.findOne({ invoice: x_id_invoice, epaycoRef: x_ref_payco })
        .then(async (doc) => {
            if (doc) {
                doc.status = x_response;
                let paymentId = doc._id;
                doc.save();
                if (x_response == 'Aceptada' && x_cod_response == '1') {
                    let exists = await Ticket.findOne({ code: x_ref_payco });
                    if (!exists) {
                        let ticket = new Ticket({
                            user: doc.userId,
                            event: doc.eventId,
                            payment: paymentId,
                            code: x_ref_payco,
                            createdAt: new Date(),
                            status: 'Por redimir'
                        });
                        let eventBuyed = await Event.findById(doc.eventId);
                        let qrOwner = await User.findById(doc.userId);
                        sendEmailWithTicketToUser(x_ref_payco, x_id_invoice, eventBuyed.name, eventBuyed.imageSrc, qrOwner.email);
                        await ticket.save();
                    }
                    res.end();
                }
            } else {
                res.end();
            }
        })
});


module.exports = paymentRouter;