const express = require('express');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuidv1 = require('uuid/v1');

const usersRouter = express.Router();
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'edancevent@gmail.com',
        pass: 'valentino83010'
    }
});

const mailOptions = {
    from: '',
    to: '',
    subject: '',
    text: ''
};


const User = require('../models/User');

/** Make a login. */
usersRouter.post('/login', async (req, res) => {
    const { email, password } = req.body;
    User.findOne({ email }).then((user) => {
        if (user) {
            try {
                if (bcrypt.compareSync(password, user.password)) {
                    let token;
                    if (req.body.isMobile) {
                        token = jwt.sign({ sub: user._id }, process.env.TOKEN_SECRET);
                    } else {
                        token = jwt.sign({ sub: user._id }, process.env.TOKEN_SECRET, { expiresIn: '4h'});
                    }
                    user = user.toObject();
                    delete user.password;
                    user.token = token;
                    res.status(200).json({ status: 'Authenticated', data: user });
                } else {
                    res.status(500).json({ status: 'An error ocurred', error: 'Invalid password'})
                }
            } catch(error2) {
                throw(error2);
            }
        } else {
            res.status(500).json({ status: 'An error ocurred', error: 'User does not exists' });
        }
        
    }).catch((error) => {
        res.status(500).json({ status: 'An error ocurred', error: error.toString() });
    });
});

/** Create new user */
usersRouter.post('/signup', async (req, res) => {
    let userLoggedEmail = req.body.loggedUserEmail || null;
    let exists = await User.findOne({email: req.body.email});
    if (!exists) {
        let newUser = new User(req.body);
        let passwordForVerificator;
        if (newUser.role == "Portero") {
            if (process.env.PRODUCTION == "true") {
                passwordForVerificator = uuidv1().slice(0, 7);
            } else {
                passwordForVerificator = "123456789";
            }
            newUser.password = passwordForVerificator;
        }
        newUser.password = bcrypt.hashSync(newUser.password, 10);
        let approvUrl = process.env.CLIENT_URL + '/users/validation';
        newUser.createdAt = new Date();
        newUser.save().then((record) => {
            if (newUser.role == "Firma") {
                mailOptions.subject = `${req.body.name} ${req.body.lastname} de ${req.body.location} quiere unirse a EDE`;
                mailOptions.to = 'mauricioatencia@edancevent.com'
                mailOptions.text = `
                ${req.body.name + ' ' + req.body.lastname} de la ciudad de ${req.body.location} ha solicitado unirse a EDE como organizador.
                
                Ingrese en el siguiente link para ver la información completa y aprobar/rechazar el usuario:
                ${approvUrl}/${record._id}`;
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.log(error);
                    }
                });
            } else if (newUser.role == "Portero") {
                mailOptions.subject = "El Usuario validador de tickets ha sido creado exitosamente!";
                mailOptions.text = `Hola, tu usuario verificador de tickets: ${newUser.name},  ha sido creado exitosamente, la contraseña de ingreso es: ${passwordForVerificator}`;
                [newUser.email, userLoggedEmail].map((user) => {
                    mailOptions.to = user;
                    transporter.sendMail(mailOptions)
                });
            }
            let token;
            if (req.body.isMobile) {
                token = jwt.sign({ sub: record._id }, process.env.TOKEN_SECRET);
            } else {
                token = jwt.sign({ sub: record._id }, process.env.TOKEN_SECRET, { expiresIn: '4h'});
            }
            record = record.toObject();
            delete record.password;
            record.token = token;
            res.status(200).json({ status: 'success', data: record });
        }).catch((error) => {
            res.status(500).json({ status: 'An error ocurred', error });
        });
    } else {
        res.status(200).json({ status: 'An error ocurred', message: 'Este usuario ya está registrado en EDE' });
    }
});

usersRouter.get('/:id', async (req, res) => {
    User.findById(req.params.id, (error, user) => {
        if (error == null) {
            res.status(200).json({ status: 'Consulted', user });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
});

/** Update an user */
usersRouter.put('/', async (req, res) => {
    const newUser = req.body;
    User.findByIdAndUpdate(newUser._id, newUser).then((newRecord) => {
        res.status(200).json({ status: 'success', message: 'User updated', data: newRecord });
    }).catch((error) => {
        res.status(500).json({ status: 'An error ocurred', error });
    });
});

usersRouter.delete('/:id', async (req, res) => {
    let userId = req.params.id;
    let user = await User.findById(userId);
    user.status = 0;
    await user.save();
    res.status(200).json({ status: 'success', message: 'User deleted' });
});

usersRouter.post('/activate', async (req, res) => {
    let userId = req.body.userId;
    let user = await Event.findById(userId);
    user.status = 2;
    await user.save();
    res.status(200).json({ status: 'success', message: 'User actived' });
});

usersRouter.post('/recoveryPassword', (req, res) => {
    let { userEmail } = req.body;
    User.findOne({ email: userEmail }, (error, user) => {
        if (user != null) {
            mailOptions.subject = 'Recupreción de password, EDE';
            mailOptions.to = userEmail;
            let token = uuidv1();
            mailOptions.text = `
            Ingresa en el siguiente link para establecer una nueva contraseña, estará habilitado por 2 hrs.

            ${process.env.CLIENT_URL}/users/changePassword?userId=${user._id}&&token=${token}
            `;
            transporter.sendMail(mailOptions);
            res.status(200).json({ status: 'Email sent' });
        } else {
            res.status(500).json({ status: 'An error ocurred', error: 'No existe un usuario con este email'});
        }
    });
});

usersRouter.post('/changePassword', (req, res) => {
    let { userId, currentPassword, isChangeMode, newPassword } = req.body;
    User.findById(userId, (error, user) => {
        if (isChangeMode) {
            if (bcrypt.compareSync(currentPassword, user.password)) {
                user.password = bcrypt.hashSync(newPassword, 10);
            } else {
                res.status(500).json({ status: 'An error ocurred', error: 'The passwords doesnt match' })
            }
        } else {
            user.password = bcrypt.hashSync(newPassword, 10);
        }
        user.save();
        res.status(200).json({ status: 'Password changed' });
    });
})

/** Get all users */
usersRouter.get('/', async (req, res) => {
    User.find({}, (error, users) => {
        if (error == null) {
            res.status(200).json({ status: 'Consulted', users });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
});

usersRouter.get('/validators/:id', async(req, res) => {
    const { id } = req.params;
    User.find({ eventId: id }, (error, users) => {
        if (error == null) {
            res.status(200).json({ status: 'Consulted', users });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    })
})

usersRouter.post('/validation', async (req, res) => {
    const { userId, validated, status } = req.body;
    User.findById(userId, (error, user) => {
        if (error == null) {
            if (validated == true) {
                user.status = status;
                mailOptions.subject = "Te damos la bienvenida a EDE!";
                mailOptions.text = `Tu usuario ha sido aprobado por un administrador, ahora puedes ingresar y empezar a crear eventos con EDE:
                ${process.env.CLIENT_URL}/login
                
                Gracias por hacer parte de EDE.`
                mailOptions.to = user.email;
                transporter.sendMail(mailOptions);
            } else {
                user.status = 0;
            }
            user.save();
            res.status(200).json({ status: 'Updated', user });
        } else {
            res.status(500).json({ status: 'An error ocurred', error });
        }
    });
});

module.exports = usersRouter;