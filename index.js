/** Server entrypoint, Main file */

// System dependencies
const http = require('http'),
      express = require('express'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      mongoose = require('mongoose');

const jwt = require('./utils/jwt');

// Setting up our environment and private variables.
const dotenv = require('dotenv');
const result = dotenv.config();
if (result.error) throw result.error

const app = express();
const port = process.env.port || 3000;

/** Express settup */
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(jwt());

const usersRouter = require('./apiRoutes/user');
app.use('/api/users', usersRouter);

const eventsRouter = require('./apiRoutes/event');
app.use('/api/events', eventsRouter);

const paymentRouter = require('./apiRoutes/payments');
app.use('/api/payments', paymentRouter);

//MongoDB Connection
const DB_NAME = process.env.DATABASE_NAME;
const DB_PORT = process.env.DATABASE_PORT;
const DB_USER = process.env.DATABASE_USER;
const DB_PWD = process.env.DATABASE_PWD;

mongoose.connect(`mongodb://${DB_USER}:${DB_PWD}@localhost:${DB_PORT}/${DB_NAME}`, { useNewUrlParser: true });

const Server = http.createServer(app);
Server.listen(port, () => console.log(`Server is up and running on port: ${port}`));