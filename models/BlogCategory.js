const mongoose = require('mongoose');

const BlogCategorySchema = mongoose.Schema({
    name: { type: String, required: true },
    imageSrc: { type: String, required: true },
    description: { type: String, required: true },
    subcategories: { type: Array }
});

module.exports = mongoose.model('BlogCategory', BlogCategorySchema);