const mongoose = require('mongoose');

const EventSchema = mongoose.Schema({
    userId: { type: String, required: true },
    name: { type: String, required: true },
    musicalGender: { type: String, required: true },
    description: { type: String, required: true },
    lineUp: { type: String, required: true },
    imageSrc: { type: String, required: true },
    tickets:{ type: [mongoose.Schema.Types.Mixed], required: false, default: null },
    location: { type: mongoose.Schema.Types.Mixed, required: true },
    eventType: { type: String, required: true },
    benefits: { type: String },
    guestlist: { type: Number, required: true },
    guestlistQuantity: { type: Number },
    guestlistUsers: { type: Array },
    warnings: { type: String },
    vjLineUp: { type: String },
    startDate: { type: String, required: true },
    startTime: { type: String, required: true },
    promoters: { type: [mongoose.Schema.Types.Mixed], default: null },
    validators: { type: [mongoose.Schema.Types.Mixed], default: null },
    endTime: { type: String, required: true },
    endDate: { type: String, required: true },
    status: { type: String, required: true }, // active - inactive
    signature: { type: String, required: true },
    createdDate: { type: String, required: true }
});

module.exports = mongoose.model('Event', EventSchema);