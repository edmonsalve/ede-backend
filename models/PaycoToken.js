const mongoose = require('mongoose');

const paycoTokenSchema = mongoose.Schema({
    userId: { type: String, required: true },
    tokenId: { type: String, required: true, unique: true },
    customerId: { type: String, required: true, unique: false },
});

module.exports = mongoose.model('PaycoToken', paycoTokenSchema);