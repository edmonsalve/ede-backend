const mongoose = require('mongoose');

const paymentSchema = mongoose.Schema({
    createdDate: { type: String, required: true },
    invoice: { type: String, required: true, unique: true },
    epaycoRef: { type: String, required: false },
    wompiRef: { type: String, required: false },
    eventId: { type: String, required: true },
    userId: { type: String, required: true },
    status: { type: String, required: true },
    entity: { type: String, required: true },
    value: { type: Number, required: true },
    pin: { type: String, required: false },
    projectCode: { type: String, required: false },
    transactionID: { type: String, required: false } // Field for PSE payments
});

module.exports = mongoose.model('Payment', paymentSchema);
