const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    category: { type: mongoose.Schema.Types.ObjectId, required: true },
    createdDate: { type: Date, default: Date.now, required: true },
    content: { type: String, required: true }
});

mongoose.model('Post', PostSchema);

module.exports = PostSchema;