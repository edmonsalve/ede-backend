const { Schema, model } = require('mongoose');

const ticketSchema = Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    event: { type: Schema.Types.ObjectId, ref: 'Event', required: true },
    payment: { type: Schema.Types.ObjectId, ref: 'Payment', required: false },
    code: { type: String, required: true, unique: true },
    status: { type: String, required: true },
    questions: { type: Schema.Types.Mixed, required: false },
    createdAt: { type: String, required: true }
});

module.exports = model('Ticket', ticketSchema);