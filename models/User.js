const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    birthDay: { type: Date },
    status: { type: Number, default: 1 }, // 0 Inactivo 1 En aprobacion 2 Activo (full access), 3 (Activo acceso promocional)
    signature: { type: String },
    gender: { type: String, required: false },
    location: { type: String, required: false },
    cellPhone: { type: String, required: false },
    expYears: { type: String, required: false },
    eventsLastYear: { type: String, required: false },
    clubOrOrganizer: { type: String, required: false },
    socialLink1: { type: String, required: false },
    socialLink2: { type: String, required: false },
    password: { type: String, default: '123456789' },
    role: { type: String, required: true },
    eventId: { type: String, default: null },
    createdAt: { type: Date, required: true }
});

UserSchema.methods.toJSON = function() {
    let obj = this.toObject();
    delete obj.password;
    return obj;
}

module.exports = mongoose.model('User', UserSchema);