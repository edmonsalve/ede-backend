"use strict"

const epaycoClient = require('epayco-node');

class EpaycoManager {
    constructor() {
        this.apiKey = process.env.EPAYCO_API_KEY;
        this.secretKey = process.env.EPAYCO_PRIVATE_KEY;
        let test = process.env.PRODUCTION == 'true' ? false : true;
        this.epaycoMngr = epaycoClient({
            apiKey: this.apiKey,
            privateKey: this.secretKey,
            lang: 'ES',
            test: test
        });
    }
    createToken(cardData) {
        return this.epaycoMngr.token.create(cardData);
    }
    createCustomer(customerInfo) {
        return this.epaycoMngr.customers.create(customerInfo);
    }
    charge(paymentInfo) {
        return this.epaycoMngr.charge.create(paymentInfo);
    }
    chargeByPSE(data) {
        return this.epaycoMngr.bank.create(data)
    }
    chargeByCash(entity, data) {
        return this.epaycoMngr.cash.create(entity, data)
    }
    getTransactionStatus(transactionId) {
        return this.epaycoMngr.bank.get(transactionId);
    }
}

module.exports = EpaycoManager;