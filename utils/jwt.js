const expressJwt = require('express-jwt');

let jwt = () => {
    const secret = process.env.TOKEN_SECRET;

    return expressJwt({ secret }).unless({
        path: [
            '/api/events/getAll',
            '/api/events/guestlist',
            '/api/users/login',
            '/api/users/signup',
            '/api/users/recoveryPassword',
            '/api/users/changePassword',
            '/api/payments/paymentValidation',
            '/api/payments/transactionStatus'
        ]
    });
}

module.exports = jwt;

