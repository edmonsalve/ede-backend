const request = require("request");


class WompiClient {

    
    constructor(environment) {

        this.sandboxBaseApiUrl = "https://sandbox.wompi.co/v1";
        this.productionApiUrl = "https://production.wompi.co/v1";
        
        this.CREDIT_CARD_METHOD = "CREDIT_CARD";
        this.NEQUI_METHOD = "NEQUI";
        
        this.baseUrl = environment == "test" ? this.sandboxBaseApiUrl : this.productionApiUrl;
        this.publicKey = environment == "test" ? process.env.WOMPI_TEST_PUBLIC_KEY : process.env.WOMPI_PRODUCTION_PUBLIC_KEY;
    }

    getTransactionStatus(transactionId) {
        let url = this.baseUrl + `/transactions/${transactionId}`;
        let options = { method: 'GET', url };

        return new Promise((resolve, reject) => {
            request(options, (error, body, response) => {
                if (error) reject(error);

                resolve({ response, body });
            });
        });
    }

    createTransaction(transactionData) {
        let url = this.baseUrl + `/transactions`;
        let options = { method: 'POST', url, json: true, headers: { authorization: `Bearer ${this.publicKey}` } };

        let payload = {
            amount_in_cents: Number(transactionData.value),
            currency: 'COP',
            customer_email: transactionData.userEmail,
            payment_method_type: transactionData.paymentMethod,
            reference: transactionData.reference
        }

        // Complete the payment data based on the paymenMethod passed
        payload.payment_method = this._setPaymentMethodPayload(transactionData);

        // Set the payload to the request options
        options.body = payload;

        return new Promise((resolve, reject) => {
            request(options, (error, body, response) => {
                if (error) reject(error);

                resolve({ response, body });
            });
        });
    }

    tokenizeCreditCard(creditCardData) {
        let url = this.baseUrl + `/tokens/cards`;
        let options = { method: 'POST', url, json: true, headers: { authorization: `Bearer ${this.publicKey}` } };

        let payload = {
            number: creditCardData.number,
            cvc: creditCardData.cvc,
            exp_month: creditCardData.expMonth,
            exp_year: creditCardData.expYear,
            card_holder: creditCardData.name
        }

        options.body = payload;

        return new Promise((resolve, reject) => {
            request(options, (error, body, response) => {
                if (error) reject(error);

                resolve({ response, body });
            });
        });
    }


    _setPaymentMethodPayload(transactionData) {
        let paymentMethodData = {};
        switch(transactionData.paymentMethod) {
            case this.CREDIT_CARD_METHOD:
                paymentMethodData = {
                    "type": "CARD",
                    "installments": transactionData.installments, // Número de cuotas
                    "token": transactionData.token // Token de la tarjeta de crédito
                }
            default:
                break;
        }
        return paymentMethodData;
    }



}

module.exports = WompiClient;